import { GET_ALERTS, TOGGLE_ALERT, ALERT_DANGER, ALERT_DEFAULT, ALERT_ERROR, ALERT_INFO, ALERT_PRIMARY, ALERT_SPECIAL, ALERT_SUCCESS, ALERT_WARNING } from '../constants';

export const getAlerts = (alerts = []) => {
  //load alerts
  return dispatch => {
    //return alerts
      dispatch({
          type : GET_ALERTS,
          payload : alerts
      });        
  };
};

export const specialAlert = (alert, alerts=[]) => {
  alerts.push(alert);
  return dispatch => {
        dispatch({
            type : ALERT_SPECIAL,
            payload : alerts
        });  
  };
};

export const warningAlert = (alert, alerts=[]) => {
  alerts.push(alert);
  return dispatch => {
        dispatch({
            type : ALERT_WARNING,
            payload : alerts
        });  
  };
};

export const successAlert = (alert, alerts=[]) => {
  alerts.push(alert);
  return dispatch => {
        dispatch({
            type : ALERT_SUCCESS,
            payload : alerts
        });  
  };
};

export const primaryAlert = (alert, alerts=[]) => {
  alerts.push(alert);
  return dispatch => {
        dispatch({
            type : ALERT_PRIMARY,
            payload : alerts
        });  
  };
};

export const infoAlert = (alert, alerts=[]) => {
  alerts.push(alert);
  return dispatch => {
        dispatch({
            type : ALERT_INFO,
            payload : alerts
        });  
  };
};

export const errorAlert = (alert, alerts=[]) => {
  alerts.push(alert);
  return dispatch => {
        dispatch({
            type : ALERT_ERROR,
            payload : alerts
        });  
  };
};

export const defaultAlert = (alert, alerts=[]) => {
  alerts.push(alert);
  return dispatch => {
        dispatch({
            type : ALERT_DEFAULT,
            payload : alerts
        });  
  };
};

export const dangerAlert = (alert, alerts=[]) => {
  alerts.push(alert);
  return dispatch => {
        dispatch({
            type : ALERT_DANGER,
            payload : alerts
        });  
  };
};


export const toggleAlert = (alerts= []) => {
  return dispatch => {
        dispatch({
            type : TOGGLE_ALERT,
            payload : alerts
        });  
  };
};
