import axios from "axios";

import HOST_URL from "../../configs";
import { BLOCKCHAIN_ERROR, BLOCKCHAIN_LAST_TEN } from '../constants';
export const getBlockchain = () => {
  return dispatch => {
    axios
      .get(`${HOST_URL}/Block`) //{headers:{Authorization:token}}
      .then(response => {
        dispatch({
          type: BLOCKCHAIN_LAST_TEN,
          payload: response
        });
      })
      .catch(err => {
        dispatch({
          type: BLOCKCHAIN_ERROR,
          payload: err
        });
      });
  };
};
