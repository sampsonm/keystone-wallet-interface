import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import logo from '../assets/images/logo.png';

class Exchange extends Component {
    render() {
      return (
  <div className="">
    <section>
 	<div className="container-fluid d-none d-md-block" id="top_area">
 		<div className="container">
 			<div className="row">
 				<div className="col text-center"><img src={logo} alt=""/></div>
 			</div>
 		</div>
 	</div>
 	<div className="container-fluid mx-0 px-0" id="navigation">
   <Header />
 	</div>
 </section>
   <div>
   Other stuffs go here!!
 </div>
   <Footer />
    </div>
    );
    }
  }
  export default withRouter(Exchange);
