import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { Row, Col, Container } from 'reactstrap';
import TermsOfService from './Modals/TermsOfService';
import PrivacyPolicy from './Modals/PrivacyPolicy';
import AddressBook from './Modals/AddressBook';
import facebook from '../assets/images/facebook.png';
import twitter from '../assets/images/twitter.png';
import instagram from '../assets/images/instagram.png';
import discord from '../assets/images/discord.png';
import forumimage from '../assets/images/forum.png';
import telegram from '../assets/images/telegram.png';
import reddit from '../assets/images/reddit.png';
import youtube from '../assets/images/youtube.png';
class Footer extends Component {
  
   getCopyright()
   {
     return(
         <footer className="color7-bg color5">
           <Container fluid>
               <Row>
                  <Col className="text-center">
                        <div className="pt-3"></div>
                     <p>&copy; Copyright 2018 Keystone Currency. All Rights Reserved.
                     <a data-toggle="modal" data-target="#termsOfServiceModal" style={{cursor:"pointer"}}>Terms</a> | <a data-toggle="modal" data-target="#privacyPolicyModal"  style={{cursor:"pointer"}}>Privacy Policy</a></p>
                  </Col>
               </Row>
           </Container>
         </footer>);
   }  
  
   render() {
        return(  
 <span name="Footer">
            <section className="py-5 color18-bg" >
               <Container fluid>
                   <Row className="align-content-justified">
                       <Col className="self-align-center text-center" sm={12} md={4}>
                           <h2 className="color5" style={{textShadow: '2px 2px 4px black'}}><strong>Keystone Blockchain</strong></h2>            
                           <ul className="list-unstyled">
                               <li>
                                   <Row>                                       
                                       <Col className="align-self-center text-center h-100">
                                          <Link to="/Block" >Explorer</Link> 
                                       </Col>                
                                   </Row>
                               </li>
                               <li>
                                   <Row>
                                       
                                       <Col className="align-self-center text-center h-100">
                                           <Link to="/Roadmap">Roadmap</Link> 
                                       </Col>                
                                   </Row>
                               </li>
                               <li>
                                   <Row>
                                       
                                       <Col className="align-self-center text-center h-100">
                                           <Link to="/FAQ">FAQ</Link> 
                                       </Col>                
                                   </Row>
                               </li>
                               <li>
                                   <Row>
                                       
                                       <Col className="align-self-center text-center h-100">
                                           <Link to="/Litepaper">Litepaper</Link> 
                                       </Col>                
                                   </Row>
                               </li>
                           </ul>
                       </Col>
                       <Col sm={12} md={4} className="text-center">
                           <h2 className="color5" style={{textShadow: '2px 2px 4px black'}}><strong>Keystone Currency</strong></h2>
                           <ul className="list-unstyled">
                               <li>
                                   <Row>
                                       <Col className="align-self-center text-center h-100">
                                           <Link to="/Wallet/Create">Create A Wallet </Link>
                                       </Col>                
                                       
                                   </Row>
                               </li>
                               <li>
                                   <Row>
                                       <Col className="align-self-center text-center h-100">
                                           <Link to="/Sign-In">Sign In</Link> 
                                       </Col>                
                                                       
                                   </Row>
                               </li>
                               <li>
                                   <Row>
                                       <Col className="align-self-center text-center h-100">
                                           <Link to="/Exchange" className="disabled">Keystone Exchange (Coming Soon)</Link> 
                                       </Col>                
                                   </Row>
                               </li>
                               <li>
                                   <Row>
                                       <Col className="align-self-center text-center h-100">
                                           <Link to="/Cart" className="disabled">Keystone Shopping Cart (Coming Soon)</Link> 
                                       </Col>                
                                   </Row>
                               </li>
                           </ul>
                           <div className="col" id="social_icons">
                <div><a href="https://www.facebook.com/KeystoneCurrency/"><img src={facebook} alt="Keystone Facebook"/></a></div>
                <div><a href="https://twitter.com/Keystone_KEYS"><img src={twitter} alt="Keystone Twitter"/></a></div>
                <div><a href="https://www.instagram.com/keystonecurrency/"><img src={instagram} alt="Keystone Instagram"/></a></div>
                <div><a href="https://discord.gg/sJvgZ8K"><img src={discord} alt="Keystone Discord"/></a></div>
                <div><a href="https://bitcointalk.org/index.php?topic=3238352.0"><img src={forumimage} alt="Keystone Bitcointalk"/></a></div>
                <div><a href="https://t.me/joinchat/AAAAAEyeJE3cAzcZjG8OHg"><img src={telegram} alt="Keystone Telegram"/></a></div>
                <div><a href="https://www.reddit.com/r/KeystoneCurrency/"><img src={reddit} alt="Keystone Reddit"/></a></div>
                <div><a href="https://www.youtube.com/channel/UClLgDTUOlBtVccz8d7I1AJQ"><img src={youtube} alt="Keystone Youtube"/></a></div>
              </div>
                       </Col>
                       <Col className="text-center" sm={12} md={4}>
                           <h2 className="color5"  style={{textShadow: '2px 2px 4px black'}}><strong>More Info</strong></h2>
                           <ul className="list-unstyled">
                               <li>
                                   <Row>
                                       <Col className="align-self-center text-center h-100">
                                           <Link to="/La-Tortuga" className="disabled">Project Tortuga(Coming Soon) </Link>
                                       </Col>                            
                                   </Row>
                               </li>
                               <li>
                                   <Row>
                                       <Col className="align-self-center text-center h-100">
                                           <Link to="#Newsletter-SignUp" onClick={()=>{alert('Newsletter Modal Popup goes here!')}}>Newsletter Sign Up </Link>
                                       </Col>                                            
                                   </Row>
                               </li>
                               <li>
                                   <Row>
                                       <Col className="align-self-center text-center h-100">
                                            <Link to="/Contact-Us">Contact Us</Link>
                                       </Col>                                            
                                   </Row>
                               </li>
                           </ul>
                       </Col>
                   </Row>
               </Container>
            </section>
        {this.getCopyright()}
          <TermsOfService />
          <PrivacyPolicy />
          <AddressBook />
        </span>
      );
    }
 }

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated
  };
};

export default withRouter(connect(mapStateToProps)(Footer));
