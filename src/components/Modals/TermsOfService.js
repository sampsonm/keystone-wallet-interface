import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
class TermsOfService extends Component {
  render(){
        return(
        <div className="modal fade" id="termsOfServiceModal" tabIndex="-1" role="dialog" aria-labelledby="termsOfServiceModalLabel" aria-hidden="true">
          <div className="pt-5 mt-5">
            <div className="modal-dialog modal-lg modal-dialog-centered " role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="termsOfServiceModalLabel">Terms Of Service</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body"><div className="color17">
                  <p className="text-justify color17">This website is for informational purposes only and does not constitute an offer or solicitation to sell shares or securities in the Company or any related or associated company.<br/>
Any such offer or solicitation will be made only by means of the Company's confidential Offering Memorandum and in accordance with the terms of all applicable securities and other laws. 
<br/>None of the information or analyses presented are intended to form the basis for any investment decision, and no specific recommendations are intended. 
<br/>Accordingly this website does not constitute investment advice or counsel or solicitation for investment in any security. 
<br/>This website does not constitute or form part of, and should not be construed as, any offer for sale or subscription of, or any invitation to offer to buy or subscribe for, any securities, nor should it or any part of it form the basis of, or be relied on in any connection with, any contract or commitment whatsoever. 
<br/>The Company expressly disclaims any and all responsibility for any direct or consequential loss or damage of any kind whatsoever arising directly or indirectly from: (i) reliance on any information contained in the website, (ii) any error, omission or inaccuracy in any such information or (iii) any action resulting therefrom. 
<br/>As set forth in the Offering Memorandum, Keystone is not responsible for the decisions made by the individuals engaged in the terms and conditions of this website or any supplemental information provided, nor is Keystone responsible for any lost private keys or any lost investment or capital positions as presented by or through the algorithm or any presented tools or platforms identified through this website or other materials presented by Keystone or its assigns and affiliates.</p>
                  <h3 className="color17">Web Site Terms and Conditions of Use</h3>
<ol>

<h4 className="color17">Terms</h4>
<li>
By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.
</li>
<li>
Use License
Permission is granted to temporarily download one copy of the materials (information or software) on Keystone Currency Inc.'s web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
modify or copy the materials;
use the materials for any commercial purpose, or for any public display (commercial or non-commercial);
attempt to decompile or reverse engineer any software contained on Keystone Currency Inc.'s web site;
remove any copyright or other proprietary notations from the materials; or
transfer the materials to another person or "mirror" the materials on any other server.
This license shall automatically terminate if you violate any of these restrictions and may be terminated by Keystone Currency Inc. at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.
</li>
<li>Disclaimer
The materials on Keystone Currency Inc.'s web site are provided "as is". Keystone Currency Inc. makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Keystone Currency Inc. does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.
</li>
<li>
Limitations
In no event shall Keystone Currency Inc. or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Keystone Currency Inc.'s Internet site, even if Keystone Currency Inc. or a Keystone Currency Inc. authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
</li><li>
Revisions and Errata
The materials appearing on Keystone Currency Inc.'s web site could include technical, typographical, or photographic errors. Keystone Currency Inc. does not warrant that any of the materials on its web site are accurate, complete, or current. Keystone Currency Inc. may make changes to the materials contained on its web site at any time without notice. Keystone Currency Inc. does not, however, make any commitment to update the materials.
</li><li>
Links
Keystone Currency Inc. has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Keystone Currency Inc. of the site. Use of any such linked web site is at the user's own risk.
</li><li>
Site Terms of Use Modifications
Keystone Currency Inc. may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.
</li><li>
Governing Law
Any claim relating to Keystone Currency Inc.'s web site shall be governed by the laws of the State of Cook Islands without regard to its conflict of law provisions.
</li><br/>
General Terms and Conditions applicable to Use of a Web Site.
<br/><br/>
Privacy Policy<br/>
Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.
<br/><br/>
Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.<br/>
We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.<br/>
We will only retain personal information as long as necessary for the fulfillment of those purposes.<br/>
We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.<br/>
Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.<br/>
We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.<br/>
We will make readily available to customers information about our policies and practices relating to the management of personal information.<br/>
We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.<br/>

</ol></div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        );
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated
  };
};


export default withRouter(connect(mapStateToProps)(TermsOfService));