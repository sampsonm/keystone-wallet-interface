import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUsers } from '../actions';
import Header from './Header';
class Users extends Component {
  componentDidMount() {
    this.props.getUsers(this.props.auth.token);
  }

  render() {
    return (
      <div>
        <Header />
        <ul>
          {this.props.users.map((user, i) => {
            return <li key={i}>{user.username}</li>;
          })}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.users,
    auth: state.auth
  };
};

export default connect(mapStateToProps, { getUsers })(Users);