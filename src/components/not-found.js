import React from 'react';
import Header from './Header';
import notfound from  '../assets/images/keystone_404_trans.png';
import Footer from './Footer';
export default (props) => {
    return(<div>
         <Header />
      <section>
        <div className="container-fluid" id="aboutus">
            <div className="container">
                <div className="row ">
                    <div className="col text-center">
                       
                        <div className="jumbotron">
                            <h1 className="green my-2 py-2"><img className="img-fluid"  src={notfound} alt=""/></h1>
                            <p className="color16">The resource my have expired or does not exist, try going back or navigating to another page.</p>
                            <p className="color12">If you continue to have this issue or think this could be a bug let us know <a href="/Contact-Us">Contact Us</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </section>
      <Footer />
        </div>);
}
