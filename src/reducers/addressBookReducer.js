import {
  GET_ADDRESS_BOOK,
  COPY_ADDRESS_TO_CLIPBOARD,
  ADD_ADDRESS_TO_ADDRESS_BOOK,
  REMOVE_ADDRESS_FROM_ADDRESS_BOOK,
  DOWNLOAD_ADDRESS_BOOK,
  DELETE_ADDRESS_BOOK, } from '../actions';

export default (addressBook = [], action) => {
    switch (action.type) {
      case DELETE_ADDRESS_BOOK:        
      case GET_ADDRESS_BOOK:      
      case COPY_ADDRESS_TO_CLIPBOARD:      
      case ADD_ADDRESS_TO_ADDRESS_BOOK:      
      case REMOVE_ADDRESS_FROM_ADDRESS_BOOK:
      case DOWNLOAD_ADDRESS_BOOK:      
        return action.payload;
      default:
        return addressBook;
    }
};