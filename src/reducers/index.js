import { combineReducers } from 'redux';
import { reducer as FormReducer } from 'redux-form';
import AddressBookReducer from './addressBookReducer';
import AlertsReducer from './alertsReducer';
import AuthReducer from './authReducer';
import BlockchainReducer from './blockchainReducer';
import CarouselReducer from './carouselReducer';
import CirculationReducer from './circulationReducer';
import CurrentBlockReducer from './currentBlockReducer';
import ErrorsReducer from './errorsReducer';
import FiltersReducer from './filtersReducer';
import ModalsReducer from './modalsReducer';
import SearchReducer from './searchReducer';
import TransactionsReducer from './transactions';
import UsersReducer from './users';

const rootReducer = combineReducers({
  form: FormReducer,
  addressBook : AddressBookReducer,
  alerts : AlertsReducer,
  auth: AuthReducer,
  chain: BlockchainReducer,
  carousels : CarouselReducer,
  circulation:CirculationReducer,
  currentBlock: CurrentBlockReducer,
  errors : ErrorsReducer,
  filters : FiltersReducer,
  modals : ModalsReducer,
  search : SearchReducer,
  transaction: TransactionsReducer,
  users: UsersReducer,
});

export default rootReducer;